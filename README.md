# SurfaceMesh
*SurfaceMesh* class. This is based of the [distmesh](http://persson.berkeley.edu/distmesh/) mesh generator by Persson and Strang and their paper [A SIMPLE MESH GENERATOR IN MATLAB](http://persson.berkeley.edu/distmesh/persson04mesh.pdf). 

## Updates 

## Demo

## Todo:

## Properties
| Function Name   | Description |
|-----------------|-------------|
| `T`  | Element connectivity list, a.k.a. the topology, is an m-by-n matrix where *m* is the number of elements and *n* is the number of nodes for the perticular element type. Each element in *T* is a vertex ID. Each row of *T* contains the vertex IDs that define an element.  |
| `P`        | Points or Vertices, specified as a matrix whose columns are the x and y coordinates of the mesh points. The row numbers of *P* are the vertex IDs in the connectivity matrix *T*.|
| `TR` | Triangulation object, see [triangulation](http://se.mathworks.com/help/matlab/ref/triangulation-class.html). If the surface is made with triangles.|
|`edges`| Edge connectivity.|
|`edgeLengths`| |
| `meanElementLength`      | |
|`aspectRatio`|Element aspect ratio. See Figure 1 for definition.|

## Functions

| Function Name   | Description |
|-----------------|-------------|
| `SurfaceMesh` | The constructor|


### SurfaceMesh() - Description

**`SM = SurfaceMesh(he,SF,bbox,Xc,optionalParameters)`**

## Figures
| Figure 1. Definition of aspect ratio.  |
|:---------------:|
| ![](http://i.imgur.com/aFeVvXw.png) |




