classdef SurfaceMesh < matlab.mixin.Copyable
    % Description
    % O = SurfaceMesh(he,SF,bbox,Xc,OptionalParameters)
    % OptionalParameters:
    %
    % maxIterations - Default is 500
    % largeDispThresh - Default is 0.1
    % Fscale - Default is 1.2
    % dt - Default is 0.2
    % deps - Default is @(he) sqrt(eps)*he
    % dispTol - Default is @(bp0,bp1) norm( bp1-bp0 )/1000
    % distributionFcn - Default is @uniformDistr
    % gradientComputation - Default is 'analytical'
    % VizMeshCreation - Default is 0
    % fixIndsFcn - Default is []
    % elementType - Default is 'P1'
    % increaseBboxBy - Default is 0.05
    
    properties
        P
        T
        TR
        edges
        edgeLengths
        meanElementLength
        aspectRatio
        InputParameters
    end
    properties(Access = private)
       bp0
       bp1
    end
    
    methods
        function O = SurfaceMesh(he,SF,bbox,Xc,varargin)
            % Description
            % O = SurfaceMesh(he,SF,bbox,Xc,OptionalParameters)
            % OptionalParameters:
            %
            % maxIterations - Default is 500
            % largeDispThresh - Default is 0.1
            % Fscale - Default is 1.2
            % dt - Default is 0.2
            % deps - Default is @(he) sqrt(eps)*he
            % dispTol - Default is @(bp0,bp1) norm( bp1-bp0 )/1000
            % distributionFcn - Default is @uniformDistr
            % gradientComputation - Default is 'analytical'
            % VizMeshCreation - Default is 0
            % fixIndsFcn - Default is []
            % elementType - Default is 'P1'
            % increaseBboxBy - Default is 0.05
            %% Input Parameters
            maxIterations = 500;
            largeDispThresh = 0.1;
            Fscale = 1.2;
            dt = 0.2;
            deps = @(he) sqrt(eps)*he;
            dispTol = @(bp0,bp1) norm( bp1-bp0 )/1000;
            distributionFcn = @uniformDistr;
            gradientComputation = 'analytical';
            VizMeshCreation = 0;
            fixIndsFcn = [];
            elementType = 'P1';
            bboxFactor = 0.05; % 5% increase
            %% Input processing
            if isenabled('maxIterations',varargin)
                ret = getoption('maxIterations', varargin);
                if ~isempty(ret); maxIterations=ret; end
            end
            
            if isenabled('largeDispThresh',varargin)
                ret = getoption('largeDispThresh', varargin);
                if ~isempty(ret); largeDispThresh=ret; end
            end
            
            if isenabled('Fscale',varargin)
                ret = getoption('Fscale', varargin);
                if ~isempty(ret); Fscale=ret; end
            end
            
            if isenabled('dt',varargin)
                ret = getoption('dt', varargin);
                if ~isempty(ret); dt=ret; end
            end
            
            if isenabled('deps',varargin)
                ret = getoption('deps', varargin);
                if ~isempty(ret); deps=ret; end
            end
            
            if isenabled('dispTol',varargin)
                ret = getoption('dispTol', varargin);
                if ~isempty(ret); dispTol=ret; end
            end
            
            if isenabled('distributionFcn',varargin)
                ret = getoption('distributionFcn', varargin);
                if ~isempty(ret); distributionFcn=ret; end
            end
            
            if isenabled('gradientComputation',varargin)
                ret = getoption('gradientComputation', varargin);
                if ~isempty(ret); gradientComputation=ret; end
            end
            
            if isenabled('VizMeshCreation',varargin)
                VizMeshCreation = 1;
            end
            
            if isenabled('fixIndsFcn',varargin)
                ret = getoption('fixIndsFcn', varargin);
                if ~isempty(ret); fixIndsFcn=ret; end
            end
            
            if isenabled('elementType',varargin)
                ret = getoption('elementType', varargin);
                if ~isempty(ret); elementType=ret; end
            end
            
            if isenabled('increaseBboxBy',varargin)
                ret = getoption('increaseBboxBy', varargin);
                if ~isempty(ret); bboxFactor=ret; end
            end
            %%
            minBsize = 0.6*min(abs(bbox(:)));
            if he > minBsize
                warning(['he is set too large for this bbox. Set he < ',num2str(minBsize)])
            end
            
            %% Expand bbox by factor of bbf
            dbbx = diff(bbox(:,1));
            dbby = diff(bbox(:,2));
            dbbz = diff(bbox(:,3));
            bbox(1,:) = bbox(1,:)-[dbbx,dbby,dbbz]*bboxFactor;
            bbox(2,:) = bbox(2,:)+[dbbx,dbby,dbbz]*bboxFactor;
            %%
            bp0 = bbox(1,:);
            bp1 = bbox(2,:);
            
            fd = SF.distanceFunction;
            fn = SF.normalsFunction;
            
            %% Create initial mesh
            nex = round((bp1(1)-bp0(1))/he);
            if mod(nex,2) == 0; nex = nex+1; end
            ney = round((bp1(2)-bp0(2))/he);
            if mod(ney,2) == 0; ney = ney+1; end
            nez = round((bp1(3)-bp0(3))/he);
            if mod(nez,2) == 0; nez = nez+1; end
            x = linspace(bp0(1),bp1(1),nex);
            y = linspace(bp0(2),bp1(2),ney);
            z = linspace(bp0(3),bp1(3),nez);
            [X,Y,Z]=ndgrid(x, y, z);
            
            
            phi = fd(X(:),Y(:),Z(:));
            PHI = reshape(phi,size(X));
            pv=isosurface(X,Y,Z,PHI,0);
            P=pv.vertices;
            T=pv.faces;
            nele = size(T,1);
            nnod = size(P,1);
            x = P(:,1); y = P(:,2);  z = P(:,3);
            
            if ~isempty(fixIndsFcn)
                [fixInds,fixDims] = fixIndsFcn(x,y,z);
            end
%             freeInds = setdiff(1:nnod,fixInds).';
            
            %% Viz initial
            if VizMeshCreation
                phandle = [];
                xfigure; axis equal tight; hold on;
                phandle = [phandle; patch('Faces',T,'Vertices',P,'FaceColor','w')];
                light; view(3)
            end
            
            %%  Connectivities (for trisurfupd)
            [t2t,t2n]=mkt2t(T);
            t2t=int32(t2t-1)'; t2n=int8(t2n-1)';
            
            %% Iterations
            Pold=inf;  % For first iteration
            ht1 = tic;
            for iter = 1:maxIterations
                P0=P;
                %% Retriangulation
                maxDisplace = max(sqrt(sum((P-Pold).^2,2))/he);
                [AR,ARN] = AspectRatio(P,T);
                
%                 max(AR)
                
                % Check for large displaments or large aspectRatio
                if maxDisplace > largeDispThresh || max(AR) > 1.7
                    Pold=P;                                          % Save current positions
                    [T,t2t,t2n]=trisurfupd(int32(T-1)',t2t,t2n,P');  % Update triangles
                    T=double(T+1)';
                    bars=[T(:,[1,2]);T(:,[1,3]);T(:,[2,3])];
                    bars=unique(sort(bars,2),'rows');
                end
                %% Move mesh points based on bar lengths L and forces F
                barvec=P(bars(:,1),:)-P(bars(:,2),:);
                L=sqrt(sum(barvec.^2,2));
%                 ARNbar = (ARN(bars(:,1))+ARN(bars(:,2)))/2;
%                 ARf = 1+ARNbar/norm(ARNbar);
                
                midBar = (P(bars(:,1),:)+P(bars(:,2),:))/2;
                hbars = distributionFcn(midBar(:,1),midBar(:,2),midBar(:,3)); %distribution evaluated at bar midpoints
                
                esp = round(max(AR));
                
                L0=hbars*Fscale*sqrt(sum(L.^2)/sum(hbars.^2));     % L0 = Desired lengths
                F=max(L0-L,0);                                     % Bar forces (scalars)
%                 Fvec=F./L*[1,1,1].*barvec.*ARf(:,[1,1,1])*1.15^esp;                        % Bar forces (x,y,z components)
                Fvec=F./L*[1,1,1].*barvec;                        % Bar forces (x,y,z components)
                Ftot=full(sparse(bars(:,[1,1,1,2,2,2]),ones(size(F))*[1,2,3,1,2,3],[Fvec,-Fvec],nnod,3));
                
                % Force = 0 at fixed points.
                if ~isempty(fixIndsFcn)
                    for ifix = 1:length(fixInds)
                        Ftot(fixInds{ifix},fixDims{ifix}) = 0;
                    end
                end
                
                
                
                dt1 = dt*1.02^esp;
                P=P+dt1*Ftot;
                
                if strcmpi(gradientComputation,'numerical')
                    %% Bring all points back to the boundary using numerical gradient
                    % that approximates the normal field.
                    % Moves the points the distance d along the normal direction
                    % This is fast.
                    %     tic
                    d = fd(P(:,1), P(:,2), P(:,3));
                    dfx = fd(P(:,1)+deps(he),P(:,2),P(:,3));
                    dfy = fd(P(:,1),P(:,2)+deps(he),P(:,3));
                    dfz = fd(P(:,1),P(:,2),P(:,3)+deps(he));
                    dfdx = (dfx-d)/deps(he); dfdy = (dfy-d)/deps(he); dfdz = (dfz-d)/deps(he);
                    dgrad2=dfdx.^2 + dfdy.^2 + dfdz.^2;
                    P=P-[d.*dfdx./dgrad2,d.*dfdy./dgrad2,d.*dfdz./dgrad2];  % Project back to boundary
                elseif strcmpi(gradientComputation,'analytical')
                    %% Bring all points back to the boundary using Exact normal
                    d = fd(P(:,1), P(:,2), P(:,3));
                    FN = fn(P(:,1), P(:,2), P(:,3));
                    dgrad2=FN(:,1).^2 + FN(:,2).^2 + FN(:,3).^2;
                    P=P-[d.*FN(:,1)./dgrad2,d.*FN(:,2)./dgrad2,d.*FN(:,3)./dgrad2];  % Project back to boundary
                else
                   error('gradientComputation must be set to either ''analytical'' or ''numerical''!') 
                end
                
                maxD = max(sqrt(sum((P-P0).^2,2))/he);
                if VizMeshCreation
                    phandle(end).Visible = 'off';
                    phandle = [phandle; patch('Faces',T,'Vertices',P,'FaceColor','w')];
                    title(['Nele: ',num2str(nele),', max displacement: ',num2str(maxD),', iteration: ',num2str(iter)])

                    phandle(end).FaceVertexCData = AR;
                    phandle(end).FaceColor = 'flat';
                    caxis([1 2]);
                    
%                     for i = 1:nele
%                        text( mean(P(T(i,:),1)), mean(P(T(i,:),2)), mean(P(T(i,:),3)), num2str(i),'BackgroundColor','y' ) 
%                     end
                    
                    drawnow
                end
                
                %% Stopping Criterion
                if maxD < dispTol(bp0,bp1)
                    disp(['Max displacement reached target displacement: ',num2str( dispTol(bp0,bp1))])
                    disp(['Elapsed time: ',num2str(toc(ht1)),' seconds.'])
                    break; 
                end
                if  max(AR) < 1.5
                    disp(['Max aspcet ratio reached target: ',num2str(1.5)])
                    break; 
                end
                
            end
            P = P+Xc(ones(nnod,1),:);
            if VizMeshCreation
                phandle(end).Visible = 'off';
                phandle = [phandle; patch('Faces',T,'Vertices',P,'FaceColor','w')];
                title(['Nele: ',num2str(nele),', max displacement: ',num2str(maxD),', iteration: ',num2str(iter)])
                phandle(end).FaceVertexCData = AR;
                phandle(end).FaceColor = 'flat';
                caxis([1 2]);
                drawnow
            end
            %%
            TR = triangulation(T,P);
            %% Create P2 Elements
            
            if strcmpi(elementType,'P2')
                disp('Converting to P2 elements...')
                tic
                [T,P] = tri2P2(TR,fd,fn);
                toc
                if VizMeshCreation
                    S = Surface(P,T,0);
                    xfigure
                    S.Visualize;
                end
            elseif strcmpi(elementType,'P1')
                %Do nothing.
            else
                error('Wrong element type. Valid element types are ''P1'' and ''P2''.')
            end
            
            %% Object creation
            O.T = T;
            O.P = P;
            O.TR = TR;
            O.edges = bars;
            O.edgeLengths = L;
            O.meanElementLength = mean(L);
            O.aspectRatio = AR;
            
            O.InputParameters.he = he;
            O.InputParameters.SF = SF;
            O.InputParameters.bbox = bbox;
            O.InputParameters.Xc = Xc;
            O.InputParameters.maxIterations = maxIterations;
            O.InputParameters.largeDispThresh = largeDispThresh;
            O.InputParameters.Fscale = Fscale;
            O.InputParameters.deps = deps;
            O.InputParameters.dt = dt;
            O.InputParameters.dispTol = dispTol;
            O.InputParameters.distributionFcn = distributionFcn;
            O.InputParameters.gradientComputation = gradientComputation;
            O.InputParameters.VizMeshCreation = VizMeshCreation;
            O.InputParameters.fixIndsFcn = fixIndsFcn;
            
            
        end
    end
    
    %Hide some of the inherited methods from handle
    methods(Hidden)
      function lh = addlistener(varargin)
         lh = addlistener@handle(varargin{:});
      end
      function notify(varargin)
         notify@handle(varargin{:});
      end
      function delete(varargin)
         delete@handle(varargin{:});
      end
      function Hmatch = findobj(varargin)
         Hmatch = findobj@handle(varargin{:});
      end
      function p = findprop(varargin)
         p = findprop@handle(varargin{:});
      end
      function TF = eq(varargin)
         TF = eq@handle(varargin{:});
      end
      function TF = ne(varargin)
         TF = ne@handle(varargin{:});
      end
      function TF = lt(varargin)
         TF = lt@handle(varargin{:});
      end
      function TF = le(varargin)
         TF = le@handle(varargin{:});
      end
      function TF = gt(varargin)
         TF = gt@handle(varargin{:});
      end
      function TF = ge(varargin)
         TF = ge@handle(varargin{:});
      end
   end
    
end

function [T,P] = tri2P2(TR,fd,fn)
    %
    %       3
    %      | \
    %      |  \
    %      |   \
    %      |    \
    %      6     5
    %      |      \
    %      |       \
    %      |        \
    %     1 --- 4 -- 2
    %
    %     Figure 1. Node numbering
    %
    T = TR.ConnectivityList;
    P = TR.Points;
    nele = size(T,1);
    edges = TR.edges;
    [nedge,s]=size(edges);
    nno=length(P);    
    P1 = zeros(nno+nedge,3);
    P1(1:nno,:) = P;
    
    tri=zeros(nele,6);
    tri(:,1:3)=T;
    nn=[1,2;2,3;3,1];
    for iel=1:nele
        nns1 = sort([T(iel,nn(:,1))',T(iel,nn(:,2))'],2);
        % The challange here is that the nodes order matters, see Figure 1.
        % This is fast.
        lind1 = edges(:,1)==nns1(1,1) & edges(:,2)==nns1(1,2);
        lind2 = edges(:,1)==nns1(2,1) & edges(:,2)==nns1(2,2);
        lind3 = edges(:,1)==nns1(3,1) & edges(:,2)==nns1(3,2);
        LI = [lind1,lind2,lind3];
        inds = (find(LI)-[0;nedge;2*nedge]).'; %Look Marv, only one find call! 
        
        newinds = nno+inds;
        it = [T(iel,:),newinds];
        tri(iel,:)=it;

        nns = edges(inds,:);
        X0 = (P(nns(:,1),:)+P(nns(:,2),:))/2;
        P1(newinds,:) = X0;        
    end
    
    %% Bring new points to surface
    P2 = P1(nno+1:end,:);
    d = fd(P2(:,1), P2(:,2), P2(:,3));
    FN = fn(P2(:,1), P2(:,2), P2(:,3));
    dgrad2=FN(:,1).^2 + FN(:,2).^2 + FN(:,3).^2;
    P2=P2-[d.*FN(:,1)./dgrad2,d.*FN(:,2)./dgrad2,d.*FN(:,3)./dgrad2];
    P1(nno+1:end,:) = P2;
    
    T = tri;
    P = P1;
end

function [AR,ARN] = AspectRatio(P,T)
%     bars=[T(:,[1,2]);T(:,[1,3]);T(:,[2,3])];
%     bars=unique(sort(bars,2),'rows');
                    
    e1m = (P(T(:,1),:)+P(T(:,2),:))/2;
    e2m = (P(T(:,2),:)+P(T(:,3),:))/2;
    e3m = (P(T(:,3),:)+P(T(:,1),:))/2;
    
    e1 = e1m - P(T(:,3),:);
    e2 = e2m - P(T(:,1),:);
    e3 = e3m - P(T(:,2),:);
    
%     e1 = P(T(:,2),:)-P(T(:,1),:);
%     e2 = P(T(:,3),:)-P(T(:,1),:);
%     e3 = P(T(:,2),:)-P(T(:,3),:);
    e1L = (e1(:,1).^2+e1(:,2).^2+e1(:,3).^2).^.5;
    e2L = (e2(:,1).^2+e2(:,2).^2+e2(:,3).^2).^.5;
    e3L = (e3(:,1).^2+e3(:,2).^2+e3(:,3).^2).^.5;
    eL = [e1L,e2L,e3L];
    eLmax = max(eL,[],2);
    eLmin = min(eL,[],2);
    AR = eLmax./eLmin;
    
%     ARB = zeros(length(bars),1);
%     for i = 1:length(bars)
%         ib = bars(i,:);
%         iT = intersect( find(any(T==ib(1),2)), find(any(T==ib(2),2)) );
%         iAR = mean(AR(iT));
%         ARB(i) = iAR;
%     end

%     nnod = length(P);
%     ARN = zeros(nnod,1);
%     for i = 1:nnod
%         iAR = AR(any(T==i,2));
%         ARN(i) = mean(iAR);
%     end
    ARN = [];
% 1;
end

function fh = uniformDistr(x,y,z)
    fh = ones(size([x,y,z],1),1);
end

function rv = isenabled(mode, varargin)
    %   ISENABLED  Checks if mode exists in the cell-array varargin.
    %
    %   isenabled(mode,varargin{:}) return true or false.
    %   example:
    %
    %          varargin = {'Viz', 'ElementNumber', 'debug', [20,20]};
    %          isenabled('debug',varargin)
    %          ans =
    %               1
    %
    %   Author: Mirza Cenanovic (mirza.cenanovic@jth.hj.se)
    %   Date: 2013-05-02
    if nargin < 1
        error('No arguments')
    end
    varargin = varargin{:};

    ind = find(strcmpi(varargin,mode), 1);
    if ~isempty(ind)
        rv = 1;
    else
        rv = 0;
    end
end

function param = getoption(mode, varargin)
    varargin = varargin{:};
    ind1 = find(strcmpi(varargin,mode), 1);
    if ~isempty(ind1)
        %Errorhandling    
        if ~iscell(varargin)
            varargin = {varargin};
        end
        if ind1+1 <= length(varargin)
            param = varargin{ind1+1};
        else
    %         error(['No options are followed by the property ''', mode,''' '])
            param = [];
        end
    else
        param = [];
    end
end

function tt = subTriag(ne,np)
    ns=ne+1;
    
    ns3=zeros(1,ns);
    ns3(1)=ns;
    ii=2;
    for i1=ns-1:-1:1
        ns3(ii)=ns3(1,ii-1)+i1;
        ii=ii+1;
    end
    
    emax = 2*ne-1; %number of triangles in last sector
    elements=1:2:emax;
    nelem = sum(elements); %number elements
    P = 1:np; %number Points
    
    si=1;
    ei=ns;
    T2=zeros(ns);
    for i1=1:ns
        T2(i1,i1:ns) = P(si:ei);
        si=ei+1;
        ei=ns3(i1)+ns-i1;
    end
    
    tri2=zeros(nelem,3);
    i1=1;
    giel=1;
    for iseq=elements
        if iseq==1
            seq1 = T2(:,[1,2]);
            ind1= seq1==0;
            seq1(ind1)=[];
            seq1 = [seq1(3),seq1(2),seq1(1)];
            tri2(1,:)=seq1;
            giel=2;
        end
        if iseq~=1
            seqi = T2(:,[i1,i1+1]);
            seqi1=seqi(:,1);
            seqi2=seqi(:,2);
            ind1= seqi1==0;
            seqi1(ind1)=[];
            c1=length(seqi1);
            
            ind2= seqi2==0;
            seqi2(ind2)=[];
            c2=length(seqi2);
            
            seqi=seqi(:);
            ind1= seqi==0;
            seqi(ind1)=[];
            seqi = seqi';
            a = c2-1;
            b = iseq - a;
            for ai=1:a
                tri2(giel,:) = seqi([ai,ai+c1+1,ai+c1]);
                giel=giel+1;
            end
            for bi=1:b
                tri2(giel,:) = seqi([bi,bi+1,bi+c1+1]);
                giel=giel+1;
            end
        end
        i1=i1+1;
    end
    tt=tri2;
end