function [fixInds,dims] = fixCylinderInds(x,y,z)

% Find nodes at borders and fix in x direction.
fixInds{1} = unique([find(x==min(x));find(x==max(x))]);
dims{1} = 1;


% Find nodes at borders and fix in x direction.
ind1 = find(x>min(x)-sqrt(eps) & x<min(x)+sqrt(eps));
ind2 = find(x>max(x)-sqrt(eps) & x<max(x)+sqrt(eps));
fixInds{1} = unique([ind1(:);ind2(:)]);
dims{1} = 1;


% xfigure; hold on; axis equal;
% plot3(x(fixInds{1}),y(fixInds{1}),z(fixInds{1}),'b*')
% 1;

end

