function [fixInds,dims] = fixSphereInds(x,y,z)

% Find nodes at borders and fix in x direction.
fixInds{1} = find(x>0-sqrt(eps) & x<0+sqrt(eps));
dims{1} = 1;

fixInds{2} = find(y>0-sqrt(eps) & y<0+sqrt(eps));
dims{2} = 2;

fixInds{3} = find(z>0-sqrt(eps) & z<0+sqrt(eps));
dims{3} = 3;

% xfigure; hold on; axis equal;
% plot3(x(fixInds{1}),y(fixInds{1}),z(fixInds{1}),'b*')
% plot3(x(fixInds{2}),y(fixInds{2}),z(fixInds{2}),'r*')
% plot3(x(fixInds{3}),y(fixInds{3}),z(fixInds{3}),'c*')
% 1;
end

