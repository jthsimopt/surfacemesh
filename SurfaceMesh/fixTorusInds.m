function [fixInds,dims] = fixTorusInds(x,y,z)

% Find nodes at midplanes and lock.
fixInds{1} = find(x>0-sqrt(eps) & x<0+sqrt(eps)); %Find nodes along x = 0
dims{1} = 1; %Lock in x dir

fixInds{2} =  find(y>0-sqrt(eps) & y<0+sqrt(eps)); %Find nodes along y = 0
dims{2} = 2; %Lock in y dir

fixInds{3} =  find(z>0-sqrt(eps) & z<0+sqrt(eps));
dims{3} = 3; %Lock in z dir
end

