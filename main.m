clear
clc
close all

AddInputPaths()



%% Sphere
% ne = 0.1;
% R = 1;
% SF = ImplicitSphere(R)
% bbox = [-R, -R, -R; R, R, R];
% Xc = [0,1,0];
% SM = SurfaceMesh(ne,SF,bbox,Xc,'VizMeshCreation','fixIndsFcn', @fixSphereInds)

%% Cylinder
% ne = 0.2;
% R = 1;
% cylinderAxis = 'x';
% SF = ImplicitCylinder(R,cylinderAxis)
% bbox = [-2, -R, -R; 2, R, R];
% Xc = [0,1,0];
% SM = SurfaceMesh(ne,SF,bbox,Xc,'VizMeshCreation','fixIndsFcn', @fixCylinderInds)

%% Torus
he = 0.2;
R = 1; r = 0.7;
SF = ImplicitTorus(R,r);
bbox = [-(R+r), -(R+r), -r; (R+r), (R+r), r];
Xc = [0,0,0];

% fh = @(x,y,z) CurvatureFunction(x,y,z,he,SF.distanceFunction);
SM = SurfaceMesh(he,SF,bbox,Xc,'VizMeshCreation', ...
    'fixIndsFcn', @fixTorusInds, 'elementType', 'P2')
% ,'distributionFcn', @(x,y,z) 0.01+0.1*((sqrt((x-0).^2+(y-0).^2))-(R-r))

return
%% Surface
S = Surface(SM.P,SM.T,0)
T = S.T; P = S.P;
xfigure

% tt1 = T(1,[1,2,3,1])
% tt2 = T(1,[4,5,6,4])
% plot3(P(tt1,1),P(tt1,2),P(tt1,3),'b-*')
% plot3(P(tt2,1),P(tt2,2),P(tt2,3),'b-o')
% P = 
S.Visualize